import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

Vue.directive('focus', {
  inserted: (el) => {
    el.focus();
  },
});

export default new Router({
  routes: [
    {
      path: '/',
      name: 'school',
      component: require('@/components/School').default,
      props: true,
    },
    {
      path: '/classroom/:id',
      name: 'classroom',
      component: require('@/components/Classroom').default,
    },
    {
      path: 'classroom/:classroomID/student/:id',
      name: 'student',
      component: require('@/components/Student').default,
    },
    {
      path: 'tc-print/:classroom/student/:id',
      name: 'tc-print',
      component: require('@/components/TcPrint').default,
    },
    {
      path: 'conduct-print/:classroom/student/:id',
      name: 'conduct-print',
      component: require('@/components/ConductPrint').default,
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});
