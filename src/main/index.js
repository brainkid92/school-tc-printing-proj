import { app, BrowserWindow, ipcMain } from 'electron' // eslint-disable-line

const shortid = require('shortid');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync('db.json');
const db = low(adapter);

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\') // eslint-disable-line
}

function yearsList() {
  const date = new Date();
  const years = [];
  const year = date.getFullYear();
  /* eslint-disable no-plusplus */
  for (let i = year; i > year - 12; i--) {
    years.push(i.toString());
  }
  return years;
}

db.defaults({
  students: [],
  classes: [
    {
      id: 1,
      name: 'STD I',
      visibility: false,
      nameInWords: 'First Standard',
    },
    {
      id: 2,
      name: 'STD II',
      visibility: false,
      nameInWords: 'Second Standard',
    },
    {
      id: 3,
      name: 'STD III',
      visibility: false,
      nameInWords: 'Third Standard',
    },
    {
      id: 4,
      name: 'STD IV',
      visibility: false,
      nameInWords: 'Fourth Standard',
    },
    {
      id: 5,
      name: 'STD V',
      visibility: false,
      nameInWords: 'Fifth Standard',
    },
    {
      id: 6,
      name: 'STD VI',
      visibility: false,
      nameInWords: 'Sixth Standard',
    },
    {
      id: 7,
      name: 'STD VII',
      visibility: false,
      nameInWords: 'Seventh Standard',
    },
    {
      id: 8,
      name: 'STD VIII',
      visibility: false,
      nameInWords: 'Eighth Standard',
    },
    {
      id: 9,
      name: 'STD IX',
      visibility: false,
      nameInWords: 'Ninth Standard',
    },
    {
      id: 10,
      name: 'STD X',
      visibility: true,
      nameInWords: 'Tenth Standard',
    },
    {
      id: 11,
      name: 'STD XI',
      visibility: false,
      nameInWords: 'Eleventh Standard',
    },
    {
      id: 12,
      name: 'STD XII',
      visibility: true,
      nameInWords: 'Twelfth Standard',
    },
  ],
  schoolName: '',
  nationalities: ['INDIAN', 'NRI'],
  religions: ['HINDU', 'CHRISTIAN', 'MUSLIM'],
  castes: ['NADAR', 'PARAVAR'],
  communities: ['OC', 'BC', 'MBC'],
  genders: ['Male', 'Female'],
  years: yearsList(),
  languages: ['TAMIL', 'ENGLISH', 'HINDI'],
  coursesOffered: [
    {
      name: 'General Education',
      subjects: ['Physics, Chemistry, Maths, Biology'],
    },
    {
      name: 'Vocational Education',
      subjects: ['TBD'],
    },
  ],
}).write();

let mainWindow;
const winURL = process.env.NODE_ENV === 'development'
  ? 'http://localhost:9080'
  : `file://${__dirname}/index.html`;

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000,
  });

  mainWindow.loadURL(winURL);

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */

/* eslint no-unused-vars: 0 */
/* eslint-disable no-console */
ipcMain.on('getSchoolName', (event, arg) => {
  event.returnValue = db.get('schoolName').value();
});

ipcMain.on('saveSchoolName', (event, data) => {
  event.returnValue = db.set('schoolName', data).write().schoolName;
});

ipcMain.on('getClassRooms', (event, data) => {
  event.returnValue = db.get('classes').value();
});

ipcMain.on('getAllNationality', (event, data) => {
  event.returnValue = db.get('nationalities').value();
});

ipcMain.on('getAllReligion', (event, data) => {
  event.returnValue = db.get('religions').value();
});

ipcMain.on('getAllCaste', (event, data) => {
  event.returnValue = db.get('castes').value();
});

ipcMain.on('getAllCommunity', (event, data) => {
  event.returnValue = db.get('communities').value();
});

ipcMain.on('getAllGender', (event, data) => {
  event.returnValue = db.get('genders').value();
});

ipcMain.on('getAllYear', (event, data) => {
  event.returnValue = db.get('years').value();
});

ipcMain.on('getAllLanguage', (event, data) => {
  event.returnValue = db.get('languages').value();
});

ipcMain.on('getAllCourseOffered', (event, data) => {
  event.returnValue = db.get('coursesOffered').value();
});

ipcMain.on('getAllGeneralSubject', (event, data) => {
  event.returnValue = db.get('coursesOffered').find({ name: 'General Education' }).value().subjects;
});

ipcMain.on('getAllVocationalSubject', (event, data) => {
  event.returnValue = db.get('coursesOffered').find({ name: 'Vocational Education' }).value().subjects;
});

ipcMain.on('saveStudent', (event, data) => {
  data.id = shortid.generate();
  event.returnValue = db.get('students').push(data).write();
});

ipcMain.on('getStudent', (event, data) => {
  event.returnValue = db.get('students').find({ id: data }).value();
});

ipcMain.on('listStudents', (event, data) => {
  event.returnValue = db.get('students').filter(data).value();
});
